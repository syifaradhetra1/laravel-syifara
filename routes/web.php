<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[\App\Http\Controllers\MahasiswaController::class,'index'])->name('mhs::index');
Route::get('/add',[\App\Http\Controllers\MahasiswaController::class,'add'])->name('mhs::add');
Route::post('/add',[\App\Http\Controllers\MahasiswaController::class,'create'])->name('mhs::create');
