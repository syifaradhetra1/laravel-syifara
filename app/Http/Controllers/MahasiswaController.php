<?php

namespace App\Http\Controllers;

use App\Models\Mahasiswa;
use Illuminate\Http\Request;

class MahasiswaController extends Controller
{
    public function index()
    {
        $mhs = Mahasiswa::all();
        return view('mahasiswa.index',[
            'mahasiswa' => $mhs,
        ]);
    }

    public function add()
    {
        return view('mahasiswa.form');
    }

    public function create(Request $request)
    {
        $mhs = new Mahasiswa();
        $mhs->nim = $request->nim;
        $mhs->nama = $request->nama;
        $mhs->save();

        return redirect()->route("mhs::index");
    }
}
