<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>    
</head>
<body>
    <h4>Data Mahasiswa</h4>
    <table border="1">
        <thead style="font-weight: bold">
            <tr>
                <td>ID</td>
                <td>NIM</td>
                <td>Nama</td>
                <td>Created at</td>
                <td>Updated at</td>        
              </tr>
        </thead>
        <tbody>
            @forelse ($mahasiswa as $mhs)
            <tr>
                <td>{{ $mhs->id }}</td>
                <td>{{ $mhs->nim }}</td>
                <td>{{ $mhs->nama }}</td>
                <td>{{ $mhs->created_at }}</td>
                <td>{{ $mhs->updated_at }}</td>              
              </tr>
            @empty
            <tr>
                <td colspan="6" style="text-align: center">Data belum tersedia</td>
              </tr>
            @endforelse
        </tbody>
      </table>
      <a href="{{route("mhs::add")}}">Tambah mahasiswa</a>
</body>
</html>
